# About TMail Domain Temporary Email System

TMail is simple to use, fast and mobile ready temporary email system with impressive feature set. It takes just 4 min to install TMail on your server. You can watch video tutorial on how to install TMail [here](https://youtu.be/2sBYzkJQ24s)

# Requirements
- Server Requirements
    - PHP >= 7.4
    - MySQL >= 5.1
    - OpenSSL PHP Extension
    - PDO PHP Extension
    - Mbstring PHP Extension
    - Tokenizer PHP Extension
    - XML PHP Extension
    - Ctype PHP Extension
    - JSON PHP Extension
    - BCMath PHP Extension
    - IMAP PHP Extension
    - iconv PHP Extension
    - ZIP PHP Extension
    - Fileinfo PHP Extension
    - Set allow_url_fopen = ON
- Email with IMAP Support
- **Default Email Forwarder (Catch all Email)**
- IMAP search ability
- **Everything** which requires Laravel 8 to run
- Enabled symlink function

**Note** : New version of TMail will not work in sub directory. You can either install TMail on Sub Domain or a Proper Domain

**Note** : TMail uses catch-all email functionality and it creates virtual email IDs for users to use and DOES NOT create actual email inbox.

# Install
**Step 1:** Duplicate .env.example and rename the copy to .env

**Step 2:** Configure the parameters in .env

**Step 3:** Visit your website to complete the installation

# Notes
Some provider maybe be able to detect your TMail and can deny your email address from being used on their websites. You can always buy additional domains to resolve this.

# Special Thanks To
devslab_co – Flutter App for TMail Customers – Purchase

Omar Abubker – Arabic Translation – Connect

waerp – German and Turkish Translation – Connect

RhoG725 – Spanish Translation – Connect

Nguyễn Hải Đăng – Vietnamese Translation – Connect

# Disclaimer: 
We don’t have any affiliation with above entities. We’re sharing just as a pure goodwill gesture.